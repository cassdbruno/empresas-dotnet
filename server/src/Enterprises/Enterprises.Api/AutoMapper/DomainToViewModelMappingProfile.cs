﻿using AutoMapper;
using Enterprises.Api.ViewModels;
using Enterprises.Domain.Enterprises;

namespace Enterprises.Api.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Enterprise, EnterpriseViewModel>();
        }        
    }
}
