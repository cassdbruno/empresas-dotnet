﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Enterprises.Api.Controllers
{
    [Produces("application/json")]
    public abstract class BaseController : Controller
    {
        protected new IActionResult Response(object result = null)
        {
            if (OperacaoValida())
            {
                return Ok(new
                {
                    success = true,
                    data = result,
                    id = "" //Obter o Id do usuário
                });
            }

            return BadRequest(new
            {
                success = false
            });
        }

        protected bool OperacaoValida()
        {
            //Neste método será verificado se há notificações de domínio para operações de gravação

            return true;
        }
    }
}