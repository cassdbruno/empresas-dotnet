﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Enterprises.Api.ViewModels;
using Enterprises.Domain.Enterprises.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Enterprises.Api.Controllers
{
    [Produces("application/json")]
    public class EnterprisesController : BaseController
    {
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IMapper _mapper;

        public EnterprisesController(IEnterpriseRepository enterpriseRepository, IMapper mapper)
        {
            _enterpriseRepository = enterpriseRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("enterprises")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllEnterprises(int version)
        {
            if (version == 2)
            {
                return Response(new { Message = "API V2 não disponível" });
            }

            try
            {
                var result = await _enterpriseRepository.GetAllEnterprisesAsync();

                var enterprises = _mapper.Map<IEnumerable<EnterpriseViewModel>>(result);

                return Response(new { enterprises });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }
            
        }

        [HttpGet]
        [Route("enterprises/{id:int}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetEnterpriseById(int id, int version)
        {
            if (version == 2)
            {
                return Response(new { Message = "API V2 não disponível" });
            }

            try
            {
                var result = await _enterpriseRepository.GetById(id);

                var enterprises = _mapper.Map<EnterpriseViewModel>(result);

                return Response(new { enterprises });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }

        }

        //enterprises/1/aQm
        [HttpGet]
        [Route("enterprises/{enterprise_types}/{name}")]
        public async Task<IActionResult> GetEnterpriseByTypeAndName(int enterprise_types, string name, int version)
        {
            if (version == 2)
            {
                return Response(new { Message = "API V2 não disponível" });
            }

            try
            {
                var enterprises = _mapper.Map<IEnumerable<EnterpriseViewModel>>(await _enterpriseRepository.GetEnterprisesByNameAndType(name, enterprise_types));

                return Response(new { enterprises });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }

        }

        //TODO: a requisição com os dados via querystring não está funcionando (não consegui descobrir o motivo)
        //[HttpGet]
        //[Route("enterprises/{enterprise_types}/{name}")]
        //public async Task<IActionResult> GetEnterpriseByTypeAndName([FromQuery]int enterprise_types, [FromQuery]string name, int version)
        //{
        //    if (version == 2)
        //    {
        //        return Response(new { Message = "API V2 não disponível" });
        //    }

        //    try
        //    {
        //        var enterprises = _mapper.Map<IEnumerable<EnterpriseViewModel>>(await _enterpriseRepository.GetEnterprisesByNameAndType(name, enterprise_types));

        //        return Response(new { enterprises });
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, new { ex.Message });
        //    }

        //}
    }
}