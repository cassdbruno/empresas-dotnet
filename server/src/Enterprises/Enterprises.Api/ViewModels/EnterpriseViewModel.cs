﻿namespace Enterprises.Api.ViewModels
{
    public class EnterpriseViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Nome fantasia
        /// </summary>
        public string TradingName { get; set; }

        /// <summary>
        /// Razão social
        /// </summary>
        public string EnterpriseName { get; set; }

        public string Cnpj { get; set; }

        public int EnterpriseType { get; set; }

        public string City { get; set; }

        public string StreetOrAvenueName { get; set; }

        public int Number { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public string State { get; set; }

        public string Complement { get; set; }
    }
}
