﻿using Enterprises.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterprises.Domain.Enterprises
{
    public class Enterprise : Entity<Enterprise>
    {
        protected Enterprise() { }

        public Enterprise(string tradingName, string enterpriseName, string cnpj)
        {
            TradingName = tradingName;
            EnterpriseName = enterpriseName;
            Cnpj = cnpj;
        }
        
        /// <summary>
        /// Nome fantasia
        /// </summary>
        public string TradingName { get; private set; }

        /// <summary>
        /// Razão social
        /// </summary>
        public string EnterpriseName { get; private set; }

        public string Cnpj { get; private set; }

        public short EnterpriseType { get; private set; }

        public string City { get; private set; }

        public string StreetOrAvenueName { get; private set; }

        public short Number { get; private set; }

        public string Country { get; private set; }

        public string ZipCode { get; private set; }

        public string State { get; private set; }

        public string Complement { get; private set; }

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        private void Validate()
        {
            ValidationResult = Validate(this);
        }
    }
}
