﻿using Enterprises.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Enterprises.Domain.Enterprises.Repository
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {
        Task<IEnumerable<Enterprise>> GetEnterprisesByNameAndType(string name, int enterpriseType);

        Task<IEnumerable<Enterprise>> GetAllEnterprisesAsync();

        Task<Enterprise> GetById(int id);
    }
}
