﻿using Enterprises.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Enterprises.Domain.Interfaces
{    public interface IRepository<TEntity> : IDisposable where TEntity : Entity<TEntity>
    {
        void Add(TEntity obj);
        IQueryable<TEntity> GetAll();
        void Update(TEntity obj);
        void Delete(int id);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        int SaveChanges();
    }
}
