﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Enterprises.IdentityServer.UserServices
{
    public class UserService : IUserService
    {
        private readonly List<User> _users = new List<User>
        {
            new User {
                SubjectId = "123",
                UserName = "testeapple",
                Password = "12341234",
                Email = "testeapple@ioasys.com.br"
            }
        };

        public User FindBySubjectId(string subjectId)
        {
            return _users.FirstOrDefault(x => x.SubjectId == subjectId);
        }

        public User FindByUsername(string username)
        {
            return _users.FirstOrDefault(x => x.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));
        }

        public bool ValidateCredentials(string username, string password)
        {
            var user = FindByUsername(username);
            if (user != null)
            {
                return user.Password.Equals(password);
            }

            return false;
        }
    }
}
