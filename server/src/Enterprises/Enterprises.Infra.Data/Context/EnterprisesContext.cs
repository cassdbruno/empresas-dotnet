﻿using Enterprises.Domain.Enterprises;
using Enterprises.Infra.Data.Extensions;
using Enterprises.Infra.Data.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Enterprises.Infra.Data.Context
{
    public class EnterprisesContext : DbContext
    {
        public EnterprisesContext(DbContextOptions<EnterprisesContext> options)
            : base(options)
        {
        }

        public DbSet<Enterprise> Enterprises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new EnterpriseMapping());

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(config.GetConnectionString("EnterprisesConnection"));
        }
    }
}
