﻿using Enterprises.Domain.Enterprises;
using Enterprises.Infra.Data.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Enterprises.Infra.Data.Mappings
{
    public class EnterpriseMapping : EntityTypeConfiguration<Enterprise>
    {
        public override void Map(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("Enterprises");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.TradingName)
              .HasColumnType("varchar(150)")
              .IsRequired();

            builder.Property(e => e.Cnpj)
              .HasColumnType("varchar(14)")
              .IsRequired();

            builder.Property(e => e.EnterpriseType)
              .HasColumnType("smallint")
              .IsRequired();

            builder.Property(e => e.City)
              .HasColumnType("varchar(100)")
              .IsRequired();

            builder.Property(e => e.StreetOrAvenueName)
              .HasColumnType("varchar(300)")
              .IsRequired();

            builder.Property(e => e.Number)
              .HasColumnType("smallint")
              .IsRequired();

            builder.Property(e => e.Country)
              .HasColumnType("varchar(100)")
              .IsRequired();

            //Configurado para CEP do Brasil
            builder.Property(e => e.ZipCode)
              .HasColumnType("varchar(8)")
              .IsRequired();

            builder.Property(e => e.State)
              .HasColumnType("varchar(100)")
              .IsRequired();

            builder.Property(e => e.Complement)
             .HasColumnType("varchar(200)")
             .IsRequired(false);

            builder.Ignore(e => e.ValidationResult);

            builder.Ignore(e => e.CascadeMode);


        }
    }
}
