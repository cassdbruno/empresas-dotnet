﻿using Enterprises.Domain.Enterprises;
using Enterprises.Domain.Enterprises.Repository;
using Enterprises.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterprises.Infra.Data.Repository
{
    public class EnterprisesRepository : Repository<Enterprise>, IEnterpriseRepository
    {
        public EnterprisesRepository(EnterprisesContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Enterprise>> GetEnterprisesByNameAndType(string name, int enterpriseType)
        {
            return await Get(e => e.EnterpriseName == name && e.EnterpriseType == (int)enterpriseType).ToListAsync();
        }

        public async Task<IEnumerable<Enterprise>> GetAllEnterprisesAsync()
        {
            return await GetAll().OrderBy(e => e.EnterpriseName).ToListAsync();
        }

        public async Task<Enterprise> GetById(int id)
        {
            return await Get(e => e.Id == id).FirstOrDefaultAsync();
        }
    }
}
