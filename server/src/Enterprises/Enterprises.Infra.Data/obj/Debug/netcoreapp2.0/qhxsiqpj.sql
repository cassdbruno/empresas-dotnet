﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Enterprises] (
    [Id] int NOT NULL IDENTITY,
    [AddressId] int NULL,
    [Cnpj] varchar(14) NOT NULL,
    [EnterpriseName] nvarchar(max) NULL,
    [EnterpriseType] tinyint NOT NULL,
    [TradingName] varchar(150) NOT NULL,
    CONSTRAINT [PK_Enterprises] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Adresses] (
    [Id] int NOT NULL IDENTITY,
    [City] varchar(100) NOT NULL,
    [Complement] varchar(200) NULL,
    [Country] varchar(100) NOT NULL,
    [EnterpriseId] int NOT NULL,
    [Number] smallint NOT NULL,
    [State] varchar(100) NOT NULL,
    [StreetOrAvenueName] varchar(300) NOT NULL,
    [ZipCode] varchar(8) NOT NULL,
    CONSTRAINT [PK_Adresses] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Adresses_Enterprises_EnterpriseId] FOREIGN KEY ([EnterpriseId]) REFERENCES [Enterprises] ([Id]) ON DELETE CASCADE
);

GO

CREATE UNIQUE INDEX [IX_Adresses_EnterpriseId] ON [Adresses] ([EnterpriseId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191021044205_1', N'2.0.3-rtm-10026');

GO

