﻿using Enterprises.Domain.Enterprises.Repository;
using Enterprises.Infra.Data.Context;
using Enterprises.Infra.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Enterprises.Infra.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASPNET
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Infra - Data
            services.AddScoped<IEnterpriseRepository, EnterprisesRepository>();
            services.AddScoped<EnterprisesContext>();
        }
    }
}
