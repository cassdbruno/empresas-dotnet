﻿using Enterprises.Domain.Enterprises.Repository;
using Enterprises.Infra.Data.Context;
using Enterprises.Infra.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Enterprises.UnitTest
{
    [TestClass]
    public class EnterprisesRepositoryTest
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        public EnterprisesRepositoryTest()
        {
            var services = new ServiceCollection();
            services.AddScoped<EnterprisesContext>();
            services.AddScoped<IEnterpriseRepository, EnterprisesRepository>();           

            var serviceProvider = services.BuildServiceProvider();

            _enterpriseRepository = serviceProvider.GetService<IEnterpriseRepository>();
        }

        [TestMethod]
        [TestCategory("EnterpriseRepository - Obter todas as empresas cadastradas")]
        public async Task GetAllEnterprises()
        {
            var enterprises = await _enterpriseRepository.GetAll().ToListAsync();

            Assert.AreEqual(enterprises.Count(), 0);
        }
    }
}
